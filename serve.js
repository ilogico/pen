// @ts-check
import { context } from 'esbuild';
import buildOptions from './buildOptions.js';
import { copyFile, mkdir } from 'fs/promises';
import path from 'path';

await mkdir('public', { recursive: true });
await Promise.all(['index.html', 'icon.svg'].map(f => copyFile(path.join('src', f), path.join('public', f))));

await context({
  ...buildOptions,
    minify: false,
    sourcemap: true,
}).then(c => c.serve({ port: 4444, servedir: 'public' }));
