// @ts-check
import { buildSync } from 'esbuild';
import buildOptions from './buildOptions.js';

buildSync({
  ...buildOptions,
  minify: true,
  treeShaking: true,
});
