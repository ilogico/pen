import { editor, KeyCode, KeyMod } from 'monaco-editor';
import { decode, encode } from './base64';

window.addEventListener('beforeunload', () => save());

window.MonacoEnvironment = {
  getWorkerUrl(_, label) {
    switch (label) {
      case 'json':
      case 'css':
      case 'html':
        return `./${label}.worker.js`;
      case 'typescript':
      case 'javascript':
        return './ts.worker.js';
      default:
        return './editor.worker.js';
    }
  }
};

editor.setTheme('vs-dark');

interface Model {
  html: string;
  javascript: string;
  css: string;
}


const defaults: Model = {
  html: '<!--html-->',
  javascript: '// javascript',
  css: '/* css */',
};

function getURLValues() {
  const searchParams = new URLSearchParams(location.hash.replace(/^#/, ''));
  const urlValues: Record<string, string> = Object.create(null);
  for (const lang of Object.keys(defaults)) {
    const value = searchParams.get(lang);
    if (value) try {
      urlValues[lang] = decode(value, { variant: 'url' });
    } catch {

    }
  }
  return urlValues;
}

const initialValues = getURLValues();

const editors = Object.fromEntries(Object.entries({ ...defaults, ...initialValues }).map(([language, value]) => {
  const el = document.getElementById(language)!;
  const ed = editor.create(el, {
    language,
    value,
    lineNumbers: 'off',
    minimap: { enabled: false },
  });
  new ResizeObserver(() => ed.layout()).observe(el);
  ed.addCommand(KeyMod.CtrlCmd | KeyCode.Enter, run);
  ed.addCommand(KeyMod.CtrlCmd | KeyCode.KeyS, save);

  return [language, ed];
}));

window.addEventListener('hashchange', () => {
  const values = getURLValues();
  for (const [lang, editor] of Object.entries(editors)) {
    if (lang in values) editor.setValue(values[lang]);
  }
});

function getModel() {
  return Object.fromEntries(Object.entries(editors).map(([lang, ed]) => [lang, ed.getValue()])) as {} as Model;
}

function run() {
  document.querySelector('iframe')!.src = html(getModel());
  save();
}

function dataURL(type: string, content: string) {
  return `data:${type};utf-8;base64,${encode(content)}`;
}
function script(text: string) {
  return `<script type="module" src="${dataURL('text/javascript', text)}"></script>`;
}

function style(text: string) {
  return `<link rel="stylesheet" href="${dataURL('text/css', text)}">`;
}
function html({ html, javascript, css }: typeof defaults) {
  return dataURL('text/html', `<!doctype html><meta charset="utf-8"><title>result</title>${style(css)}${script(javascript)}${html}`);
}

function save(copy = false) {
  const search = new URLSearchParams(Object.entries(getModel()).map(([lang, text]) => [lang, encode(text, { variant: 'url' })]));
  const url = new URL(location.href);
  url.hash = search.toString().replace(/^\??/, '#');
  const href = url.toString()
  if(copy) navigator.clipboard.writeText(href);
  history.replaceState(null, '', href);
}

const actions: Record<string, () => void> = {
  run,
  save: () => save(true),
};

document.getElementById('toolbar')!.addEventListener('click', e => {
  const target = e.target as HTMLElement;
  const { action } = target.dataset;
  if (action && Object.prototype.hasOwnProperty.call(actions, action)) {
    actions[action]();
  }
});
