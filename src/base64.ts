const standardDictionary = Uint8Array.from(
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'.split('').map(c => c.charCodeAt(0)),
);

const urlDictionary = standardDictionary.slice();
urlDictionary[62] = '-'.charCodeAt(0);
urlDictionary[63] = '_'.charCodeAt(0);

const dictionaries = {
  standard: standardDictionary,
  url: urlDictionary,
};

const paddingCode = '='.charCodeAt(0);

export function encode(
  text: string,
  { variant = 'standard', padding = variant === 'standard' }: { variant?: keyof typeof dictionaries, padding?: boolean } = {}
) {

  const dictionary = dictionaries[variant];

  const input = new TextEncoder().encode(text);
  const outputLength = padding
    ? Math.ceil(input.length / 3) * 4
    : Math.ceil(input.length * 4 / 3);
  const output = new Uint8Array(outputLength);

  let i = 0, o = 0;
  for (const end = (input.length / 3 | 0) * 3; i < end; i += 3) {
    const i0 = input[i], i1 = input[i + 1], i2 = input[i + 2];

    output[o++] = dictionary[i0 >>> 2];
    output[o++] = dictionary[((i0 & 0x3) << 4) | (i1 >>> 4)];
    output[o++] = dictionary[((i1 & 0xf) << 2) | (i2 >>> 6)];
    output[o++] = dictionary[i2 & 0x3f];
  }

  if (i < input.length) {
    const i0 = input[i++];
    const i1 = i < input.length ? input[i] : 0;

    output[o++] = dictionary[i0 >>> 2];
    output[o++] = dictionary[((i0 & 0x3) << 4) | (i1 >>> 4)];

    if (i < input.length) output[o++] = dictionary[(i1 & 0xf) << 2];
  }

  if (padding) while (o < outputLength) output[o++] = paddingCode;

  return new TextDecoder().decode(output);
}

const reverseDictionaries: Record<string, Record<string, number>> = Object.fromEntries(
  Object.entries(dictionaries).map(([variant, dictionary]) => [
    variant,
    Object.assign(
      Object.create(null),
      Object.fromEntries([...dictionary.entries()].map(([value, charCode]) => [String.fromCharCode(charCode), value])),
    ),
  ]),
);

function invalidBase64(): never {
  throw new TypeError('Invalid base64 string');
}

export function decode(
  data: string,
  { variant = 'standard' }: { variant?: 'standard' | 'url' } = {},
) {

  const dictionary = reverseDictionaries[variant];
  function lookup(char: string) {
    if (!(char in dictionary)) invalidBase64();
    return dictionary[char];
  }

  let { length } = data;
  let leftover = length % 4;
  if (leftover === 1) invalidBase64();

  let outputLength = length * 3 >> 2;
  if (leftover === 0 && length > 0 && data[length - 1] === '=') {
    leftover = 3;
    length--;
    outputLength--;
    if (data[length - 1] === '=') {
      leftover--;
      length--;
      outputLength--;
    }
  }

  let i = 0;
  let o = 0;

  const result = new Uint8Array(outputLength);
  const evenEnd = length >> 2 << 2;
  while (i < evenEnd) {
    const a = lookup(data[i++]),
      b = lookup(data[i++]),
      c = lookup(data[i++]),
      d = lookup(data[i++]);
    result[o++] = a << 2 | b >>> 4;
    result[o++] = (b & 0xf) << 4 | c >>> 2;
    result[o++] = (c & 3) << 6 | d;
  }

  if (leftover === 3) {
    const a = lookup(data[i++]),
      b = lookup(data[i++]),
      c = lookup(data[i]);

    result[o++] = a << 2 | b >>> 4;
    result[o] = (b & 0xf) << 4 | c >>> 2;
  } else if (leftover === 2) {
    const a = lookup(data[i++]),
      b = lookup(data[i++]);
    result[o++] = a << 2 | b >>> 4;
  }

  return new TextDecoder().decode(result);
}
