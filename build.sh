#!/bin/sh
set -e

node build.js
cp src/index.html src/icon.svg public/
find public -type f -exec gzip -k {} +
