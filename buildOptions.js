// @ts-check
import * as path from 'node:path';

/** @type {import('esbuild').BuildOptions} */
const buildOptions = {
  platform: 'browser',
  target: 'esnext',
  format: 'iife',
  bundle: true,
  entryPoints: [
    'main.ts',
    'ts.worker.ts',
    'html.worker.ts',
    'css.worker.ts',
    'json.worker.ts',
    'editor.worker.ts',
    'style.css',
  ].map(entry => path.join('src', entry)),
  outdir: 'public',
  sourcemap: true,
  loader: {
    '.ttf': 'file',
  },
};

export default buildOptions;
